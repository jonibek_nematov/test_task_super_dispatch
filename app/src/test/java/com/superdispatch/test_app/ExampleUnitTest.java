package com.superdispatch.test_app;

import com.superdispatch.test_app.model.Author;
import com.superdispatch.test_app.model.Jokes;
import com.superdispatch.test_app.model.Post;
import com.superdispatch.test_app.utils.DateFormatter;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void date_formatter_is_correct() {
        Date date = new Date(0);
        String result = DateFormatter.formatDate(date);
        String expected = "January 01,1970 00:00";
        assertEquals(expected, result);
    }

    @Test
    public void author_name_is_not_empty() {
        Author author = Author.getRandomAuthor();
        assertNotEquals(author, null);
        assertNotEquals(author.getName(), null);
        assert author.getPoster() > 0;
    }

    @Test
    public void test_post_fields() {
        Post post = Post.getPost(1);
        assertEquals(post.getId(), 1);
        assertEquals("Joke #1", post.getTitle());
        assertNotEquals(post.getPublishedAt(), null);
    }

    @Test
    public void test_jokes_size() {
        assertNotEquals(Jokes.jokes, null);
        assert Jokes.jokes.length > 0;
    }

    @Test
    public void test_authors_size() {
        assertNotEquals(Author.authors, null);
        assert Author.authors.length > 0;
    }
}