package com.superdispatch.test_app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superdispatch.test_app.R;
import com.superdispatch.test_app.database.IdUtilInterface;
import com.superdispatch.test_app.model.Post;
import com.superdispatch.test_app.view_holder.PostViewHolder;

import java.util.ArrayList;

public class PostRecyclerAdapter extends RecyclerView.Adapter<PostViewHolder> implements IdUtilInterface {


    private ArrayList<Post> objectArrayList;

    private Context context;

    public PostRecyclerAdapter(Context context) {
        this.context = context;
        objectArrayList = new ArrayList<>();
    }

    public synchronized void addItems(ArrayList<Post> newObjects) {
        int insertedPosition = objectArrayList.size();
        objectArrayList.addAll(newObjects);
        notifyItemInserted(insertedPosition);
    }

    public synchronized void addNewItems(ArrayList<Post> newPosts) {
        for (int j = 0; j < newPosts.size(); j++) {
            objectArrayList.add(j, newPosts.get(j));
            notifyItemRangeChanged(0, objectArrayList.size() - 1);
        }
    }

    public void clearData() {
        objectArrayList = new ArrayList<>();
        notifyDataSetChanged();

    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rc_item_post, parent, false);
        return new PostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        holder.setValues(objectArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return objectArrayList.size();
    }

    @Override
    public synchronized long getLastId() {
        if (objectArrayList != null && objectArrayList.size() > 0) {
            return objectArrayList.get(objectArrayList.size() - 1).getId();
        } else {
            return 0;
        }
    }

    @Override
    public synchronized long getFirstId() {
        if (objectArrayList != null && objectArrayList.size() > 0) {
            return objectArrayList.get(0).getId();
        } else {
            return 0;
        }
    }

}
