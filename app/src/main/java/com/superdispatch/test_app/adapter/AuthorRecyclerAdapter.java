package com.superdispatch.test_app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superdispatch.test_app.R;
import com.superdispatch.test_app.database.IdUtilInterface;
import com.superdispatch.test_app.model.Author;
import com.superdispatch.test_app.view_holder.AuthorViewHolder;

import java.util.ArrayList;

public class AuthorRecyclerAdapter extends RecyclerView.Adapter<AuthorViewHolder> implements IdUtilInterface {


    private ArrayList<Author> objectArrayList;

    private Context context;

    public AuthorRecyclerAdapter(Context context) {
        this.context = context;
        objectArrayList = new ArrayList<>();
    }


    public synchronized void addItems(ArrayList<Author> newObjects) {
        int insertedPosition = objectArrayList.size();
        objectArrayList.addAll(newObjects);
        notifyItemInserted(insertedPosition);
    }

    public void clearData() {
        final int size = objectArrayList.size();
        objectArrayList.clear();
        notifyItemRangeRemoved(0, size);
    }

    @Override
    public AuthorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rc_author, parent, false);
        return new AuthorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AuthorViewHolder holder, int position) {
        holder.setValues(objectArrayList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return objectArrayList.size();
    }

    @Override
    public synchronized long getLastId() {
        if (objectArrayList != null && objectArrayList.size() > 0) {
            return objectArrayList.get(objectArrayList.size() - 1).getId();
        } else {
            return 0;
        }
    }

    @Override
    public synchronized long getFirstId() {
        if (objectArrayList != null && objectArrayList.size() > 0) {
            return objectArrayList.get(0).getId();
        } else {
            return 0;
        }
    }

}
