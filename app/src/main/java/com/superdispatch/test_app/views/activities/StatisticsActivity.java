package com.superdispatch.test_app.views.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.superdispatch.test_app.R;
import com.superdispatch.test_app.adapter.AuthorRecyclerAdapter;
import com.superdispatch.test_app.database.AuthorDao;
import com.superdispatch.test_app.database.AuthorDaoImpl;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_statistics)
public class StatisticsActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener {

    @ViewById
    Toolbar toolbar;

    AuthorDao authorDao;

    @ViewById
    RecyclerView rcAuthors;

    @ViewById
    SwipeRefreshLayout srlAuthors;

    AuthorRecyclerAdapter authorRecyclerAdapter;

    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    void afterViews() {
        srlAuthors.setOnRefreshListener(this);
        authorDao = new AuthorDaoImpl();
        toolbar.setTitle(R.string.statistics);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        setupRecyclerViewAndAdapter();
    }

    void setupRecyclerViewAndAdapter() {
        linearLayoutManager = new LinearLayoutManager(this);
        rcAuthors.setLayoutManager(linearLayoutManager);
        authorRecyclerAdapter = new AuthorRecyclerAdapter(this);
        rcAuthors.setAdapter(authorRecyclerAdapter);
        getAuthors();
    }

    void getAuthors() {
        //Handler is used to imitate server connection
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                srlAuthors.setRefreshing(false);
                authorRecyclerAdapter.addItems(authorDao.getAllAuthorsSortedByPostCount());
            }
        }, 1000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        authorRecyclerAdapter.clearData();
        getAuthors();
    }
}
