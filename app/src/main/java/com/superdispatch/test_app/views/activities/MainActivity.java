package com.superdispatch.test_app.views.activities;

import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.superdispatch.test_app.R;
import com.superdispatch.test_app.adapter.PostRecyclerAdapter;
import com.superdispatch.test_app.custom_views.ConnectionFailView;
import com.superdispatch.test_app.database.DatabaseLoaderListener;
import com.superdispatch.test_app.database.PostDaoImpl;
import com.superdispatch.test_app.model.Post;
import com.superdispatch.test_app.utils.ScrollListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import static java.lang.String.format;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements
        ConnectionFailView.OnReloadClickListener,
        DatabaseLoaderListener, SwipeRefreshLayout.OnRefreshListener {

    @ViewById
    RecyclerView rcPost;

    @ViewById
    SwipeRefreshLayout srlPosts;

    @ViewById
    TextView tvNoData;

    @ViewById
    ConnectionFailView connectionFailView;

    @ViewById
    Button btnNewPosts;

    @ViewById
    ProgressBar progressBar;

    private PostDaoImpl postDaoImpl;

    private PostRecyclerAdapter postRecyclerAdapter;

    private LinearLayoutManager linearLayoutManager;


    @AfterViews
    void afterViews() {
        postDaoImpl = new PostDaoImpl();
        setupRecyclerViewAndAdapter();
        setListeners();
        simulateConnectionError();
    }

    private void setupRecyclerViewAndAdapter() {
        linearLayoutManager = new LinearLayoutManager(this);
        rcPost.setLayoutManager(linearLayoutManager);
        postRecyclerAdapter = new PostRecyclerAdapter(this);
        rcPost.setAdapter(postRecyclerAdapter);
    }

    private void setListeners() {
        postDaoImpl.setDatabaseLoaderListener(this);
        postDaoImpl.setIdUtilInterface(postRecyclerAdapter);
        srlPosts.setOnRefreshListener(this);
        connectionFailView.setOnReloadClickListener(this);
        rcPost.addOnScrollListener(new ScrollListener(linearLayoutManager) {
            @Override
            public void loadData() {
                getDataFromDatabase();
            }
        });
    }

    //SimulatingConnectionError
    void simulateConnectionError() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideProgressBar();
                showConnectionFailView();
            }
        }, 1000);
    }


    @Override
    public void onReloadClick() {
        getDataFromDatabase();
    }

    @UiThread
    public void hideConnectionFailView() {
        connectionFailView.setVisibility(View.GONE);
    }

    @UiThread
    public void showConnectionFailView() {
        connectionFailView.setVisibility(View.VISIBLE);
    }

    @UiThread
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @UiThread
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @UiThread
    public void hideNewPostsButton() {
        btnNewPosts.setVisibility(View.GONE);
    }

    @UiThread
    public void showNewPostsButton() {
        btnNewPosts.setVisibility(View.VISIBLE);
    }

    private synchronized void getDataFromDatabase() {
        hideConnectionFailView();
        showProgressBar();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (postDaoImpl != null) {
                    postDaoImpl.getNextPage();
                } else {
                    showConnectionFailView();
                }
            }
        }, 1000);
    }


    @Override
    @UiThread
    public void onNewPostsLoaded(ArrayList<Post> newPosts) {
        hideProgressBar();
        if (postRecyclerAdapter != null)
            postRecyclerAdapter.addNewItems(newPosts);
        rcPost.smoothScrollToPosition(0);
    }

    @Override
    @UiThread
    public void onPostsLoaded(ArrayList<Post> oldPosts) {
        hideProgressBar();
        if (postRecyclerAdapter != null && oldPosts != null)
            if (oldPosts.size() > 0) {
                hideNoDataError();
                postRecyclerAdapter.addItems(oldPosts);
            } else if (postRecyclerAdapter.getItemCount() == 0)
                showNoDataError();
    }

    @UiThread
    void showNoDataError() {
        tvNoData.setVisibility(View.VISIBLE);
    }

    @UiThread
    void hideNoDataError() {
        tvNoData.setVisibility(View.GONE);
    }

    @Override
    public void onAvailableNewPost(long newPostCount) {
        btnNewPosts.setText(format(getResources().getQuantityString(R.plurals.new_jokes, (int) newPostCount), newPostCount));
        showNewPostsButton();
    }

    @Click(R.id.btnNewPosts)
    void loadNewPosts() {
        hideNewPostsButton();
        showProgressBar();
        postDaoImpl.getNewPost();
    }

    @Click(R.id.btnClear)
    void clearPosts() {
        postRecyclerAdapter.clearData();
        postDaoImpl.clearAllPosts();
        getDataFromDatabase();
    }

    @Click(R.id.btnAddPost)
    void addPost() {
        postDaoImpl.generatePost();
    }

    @Click(R.id.btnStatistics)
    void btnStatistics() {
        if (postDaoImpl.getCount() > 0) {
            StatisticsActivity_.intent(this).start();
        } else {
            Snackbar.make(rcPost, R.string.no_data, 3000).show();
        }
    }

    @Override
    public void onRefresh() {
        postRecyclerAdapter.clearData();
        srlPosts.setRefreshing(false);
        hideNewPostsButton();
        getDataFromDatabase();
    }

}

