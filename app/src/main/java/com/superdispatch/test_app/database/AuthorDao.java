package com.superdispatch.test_app.database;


import com.superdispatch.test_app.model.Author;

import java.util.ArrayList;

public interface AuthorDao {

    Author getAuthorById(long id);

    long getRandomAuthorId();

    ArrayList<Author> getAllAuthors();

    ArrayList<Author> getAllAuthorsSortedByPostCount();

    void saveAuthorsToDatabase();

    long getAuthorCount();

}
