package com.superdispatch.test_app.database;

import android.os.Handler;

public class DatabasePostGenerator extends Handler implements Runnable {


    private PostDao postDao;
    private AuthorDao authorDao;

    public DatabasePostGenerator() {
        authorDao = new AuthorDaoImpl();
        postDao = new PostDaoImpl();
    }

    public void generateData() {
        if (authorDao.getAuthorCount() == 0) {
            authorDao.saveAuthorsToDatabase();
        }
        postDelayed(this, 1000);
    }


    @Override
    public synchronized void run() {
        if (postDao.getCount() <= 100) {
            for (int i = 0; i < 5; i++)
                if (postDao.getCount() <= 99)
                    postDao.generatePost();
        }
        generateData();
    }
}
