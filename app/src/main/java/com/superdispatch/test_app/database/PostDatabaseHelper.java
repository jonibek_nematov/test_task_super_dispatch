package com.superdispatch.test_app.database;


import com.superdispatch.test_app.model.Author;
import com.superdispatch.test_app.model.Post;

import java.util.ArrayList;
import java.util.Objects;

import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

class PostDatabaseHelper {

    //Returns last id and increments it
    public long getIdForNextPost() {
        RealmResults<Post> posts = RealmSingleton.INSTANCE.getRealmInstance().where(Post.class).findAll();
        if (posts.size() > 0) {
            return Objects.requireNonNull(posts.get(posts.size() - 1)).getId() + 1;
        } else {
            //Posts are empty it starts from post number 1;
            return 1;
        }
    }

    //Returns all posts count
    public long getAllPostsCount() {
        return RealmSingleton.INSTANCE.getRealmInstance().where(Post.class).findAll().size();
    }


    //Returns all posts count of author
    public long getAllPostsCount(long authorId) {
        return RealmSingleton.INSTANCE.getRealmInstance().where(Post.class).equalTo("authorId",authorId).findAll().size();
    }

    //Save new post in to database
    public synchronized void savePostToDatabase(final Post post) {
        Realm realm = RealmSingleton.INSTANCE.getRealmInstance();
        try {
            if (post.getId() > 0)
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        Post newPost = realm.createObject(Post.class, post.getId());
                        newPost.setAuthorId(post.getAuthorId());
                        newPost.setPublishedAt(post.getPublishedAt());
                        newPost.setContent(post.getContent());
                        newPost.setTitle(post.getTitle());
                        realm.copyToRealmOrUpdate(newPost);
                        updatePostCount(post.getAuthorId(), realm);

                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Each time when new post added, author statistics are updated
    private void updatePostCount(long authorId, Realm realm) {
        long count = realm.where(Post.class).equalTo("authorId", authorId).count();
        Author author = realm.where(Author.class).equalTo("id", authorId).findFirst();
        if (author != null) {
            author.setPostCount(count);
            realm.copyToRealm(author);
        }
    }

    //Returns next 10 or less posts
    public synchronized ArrayList<Post> getNextItemsByLastId(long id, int resultLimit) {
        if (id == 0) {
            return null;
        }
        ArrayList<Post> resultList = new ArrayList<>();
        //While is used because realm.io does not support query result limitation
        while (resultLimit > 0) {
            Post post = RealmSingleton.INSTANCE.getRealmInstance().where(Post.class).sort("id", Sort.DESCENDING).lessThan("id", id).findFirst();
            if (post != null) {
                id = post.getId();
                resultList.add(post);
            } else {
                break;
            }
            resultLimit--;
        }
        return resultList;
    }

    //Returns next 10 or less posts
    public synchronized ArrayList<Post> getNextItemsByLastIdAndAuthor(long authorId, long postId, int resultLimit) {
        if (postId == 0) {
            return null;
        }
        ArrayList<Post> resultList = new ArrayList<>();
        //While is used because realm.io does not support query result limitation
        while (resultLimit > 0) {
            Post post = RealmSingleton.INSTANCE.getRealmInstance().where(Post.class).sort("id", Sort.DESCENDING).lessThan("id", postId).equalTo("authorId", authorId).findFirst();
            if (post != null) {
                postId = post.getId();
                resultList.add(post);
            } else {
                break;
            }
            resultLimit--;
        }
        return resultList;
    }


    //Variable is not local because of GC;
    private RealmResults<Post> realmResultsPosts;

    //Listener set to observe changes in Post.class table
    public void setNewPostListener(OrderedRealmCollectionChangeListener<RealmResults<Post>> newPostListener) {
        realmResultsPosts = RealmSingleton.INSTANCE.getRealmInstance().where(Post.class).findAll();
        realmResultsPosts.addChangeListener(newPostListener);
    }

    //Returns new post's count
    public long getNewPostsCount(long id) {
        return RealmSingleton.INSTANCE.getRealmInstance().where(Post.class).greaterThan("id", id).count();
    }


    //Returns new post
    public synchronized ArrayList<Post> getNewPostsByAuthorId(long id) {
        RealmResults<Post> newPosts = RealmSingleton.INSTANCE.getRealmInstance().where(Post.class).greaterThan("id", id).sort("id", Sort.DESCENDING).findAll();
        return new ArrayList<>(newPosts);
    }

    //Returns new post
    public synchronized ArrayList<Post> getNewPostsByAuthorId(long authorId, long postId) {
        RealmResults<Post> newPosts = RealmSingleton.INSTANCE.getRealmInstance().where(Post.class).equalTo("authorId", authorId).greaterThan("id", postId).sort("id", Sort.DESCENDING).findAll();
        return new ArrayList<>(newPosts);
    }

    //Removes all posts and removes author's statistics
    public void removeAllPosts() {
        RealmSingleton.INSTANCE.getRealmInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(Post.class);
                for (Author author : new AuthorDatabaseHelper().getAllAuthors()) {
                    author.setPostCount(0);
                    realm.copyToRealm(author);
                }
            }
        });
    }
}
