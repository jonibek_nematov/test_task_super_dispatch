package com.superdispatch.test_app.database;


import com.superdispatch.test_app.model.Post;

import java.util.ArrayList;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;

public class PostDaoImpl implements PostDao, OrderedRealmCollectionChangeListener<RealmResults<Post>> {

    private PostDatabaseHelper postDatabaseHelper;
    private int itemsPerPage = 10;
    private IdUtilInterface idUtilInterface;

    private DatabaseLoaderListener databaseLoaderListener;

    public PostDaoImpl() {
        postDatabaseHelper = new PostDatabaseHelper();
    }

    public void setIdUtilInterface(IdUtilInterface idUtilInterface) {
        this.idUtilInterface = idUtilInterface;
        postDatabaseHelper.setNewPostListener(this);
    }

    public void setDatabaseLoaderListener(DatabaseLoaderListener databaseLoaderListener) {
        this.databaseLoaderListener = databaseLoaderListener;
    }

    @Override
    public synchronized void getNextPage() {
        long lastId = idUtilInterface.getLastId();
        if (lastId == 0) {
            lastId = postDatabaseHelper.getIdForNextPost();
        }
        ArrayList<Post> loadedPosts = postDatabaseHelper.getNextItemsByLastId(lastId, itemsPerPage);
        if (databaseLoaderListener != null) {
            databaseLoaderListener.onPostsLoaded(loadedPosts);
        }
    }

    @Override
    public synchronized void getNextPageByAuthor(long authorId) {
        long lastId = idUtilInterface.getLastId();
        if (lastId == 0) {
            lastId = postDatabaseHelper.getIdForNextPost();
        }
        ArrayList<Post> loadedPosts = postDatabaseHelper.getNextItemsByLastIdAndAuthor(authorId, lastId, itemsPerPage);
        if (databaseLoaderListener != null) {
            databaseLoaderListener.onPostsLoaded(loadedPosts);
        }

    }

    private synchronized void requestNewPostsNumber() {
        if (databaseLoaderListener != null) {
            long newItemsCount = postDatabaseHelper.getNewPostsCount(idUtilInterface.getFirstId());
            if (newItemsCount > 0)
                databaseLoaderListener.onAvailableNewPost(newItemsCount);
        }
    }

    @Override
    public synchronized void getNewPost() {
        if (databaseLoaderListener != null) {
            ArrayList<Post> newPosts = postDatabaseHelper.getNewPostsByAuthorId(idUtilInterface.getFirstId());
            if (newPosts != null && newPosts.size() > 0) {
                databaseLoaderListener.onNewPostsLoaded(newPosts);
            }
        }
    }

    @Override
    public synchronized void getNewPostsByAuthor(long authorId) {
        if (databaseLoaderListener != null) {
            ArrayList<Post> newPosts = postDatabaseHelper.getNewPostsByAuthorId(authorId, idUtilInterface.getFirstId());
            if (newPosts != null && newPosts.size() > 0) {
                databaseLoaderListener.onNewPostsLoaded(newPosts);
            }
        }
    }


    @Override
    public synchronized long getCountByAuthor(long authorId) {
        return postDatabaseHelper.getAllPostsCount();
    }

    @Override
    public void clearAllPosts() {
        if (postDatabaseHelper != null)
            postDatabaseHelper.removeAllPosts();
    }


    @Override
    public synchronized void generatePost() {
        if (postDatabaseHelper != null) {
            long lastNumber = postDatabaseHelper.getIdForNextPost();
            postDatabaseHelper.savePostToDatabase(Post.getPost(lastNumber));
        }
    }

    @Override
    public long getCount() {
        return postDatabaseHelper.getAllPostsCount();
    }


    @Override
    public void onChange(RealmResults<Post> posts, OrderedCollectionChangeSet changeSet) {
        requestNewPostsNumber();
    }
}
