package com.superdispatch.test_app.database;

import com.superdispatch.test_app.model.Author;

import java.util.ArrayList;

public class AuthorDaoImpl implements AuthorDao {

    private AuthorDatabaseHelper authorDatabaseHelper;

    public AuthorDaoImpl() {
        authorDatabaseHelper = new AuthorDatabaseHelper();
    }

    @Override
    public Author getAuthorById(long id) {
        return authorDatabaseHelper.getAuthorById(id);
    }

    @Override
    public long getRandomAuthorId() {
        return authorDatabaseHelper.getRandomAuthorId();
    }

    @Override
    public ArrayList<Author> getAllAuthors() {
        return authorDatabaseHelper.getAllAuthors();
    }

    @Override
    public ArrayList<Author> getAllAuthorsSortedByPostCount() {
        return authorDatabaseHelper.getAllAuthorsSortedByPostCount();
    }

    @Override
    public void saveAuthorsToDatabase() {
        authorDatabaseHelper.generateAuthors();
    }

    @Override
    public long getAuthorCount() {
        return authorDatabaseHelper.getAuthorCount();
    }
}
