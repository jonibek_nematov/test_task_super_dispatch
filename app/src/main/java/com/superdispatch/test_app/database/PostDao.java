package com.superdispatch.test_app.database;


public interface PostDao {

    void getNextPage();

    void getNewPost();

    void getNewPostsByAuthor(long authorId);

    void getNextPageByAuthor(long authorId);

    long getCountByAuthor(long authorId);

    void clearAllPosts();

    void generatePost();

    long getCount();
}
