package com.superdispatch.test_app.database;

import io.realm.Realm;

public enum  RealmSingleton {
    INSTANCE;


    RealmSingleton() {
    }

    private Realm realm = Realm.getDefaultInstance();


    public Realm getRealmInstance() {
        return realm;
    }
}
