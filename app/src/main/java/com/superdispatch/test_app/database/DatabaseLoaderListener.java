package com.superdispatch.test_app.database;


import com.superdispatch.test_app.model.Post;

import java.util.ArrayList;

public interface DatabaseLoaderListener {

    void onNewPostsLoaded(ArrayList<Post> newPosts);

    void onPostsLoaded(ArrayList<Post> oldPosts);

    void onAvailableNewPost(long newPostCount);
}
