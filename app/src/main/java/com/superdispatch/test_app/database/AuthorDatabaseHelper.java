package com.superdispatch.test_app.database;


import com.superdispatch.test_app.model.Author;
import com.superdispatch.test_app.utils.AuthorGeneraterUtil;

import java.util.ArrayList;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

class AuthorDatabaseHelper {

    public long getAuthorCount() {
        return RealmSingleton.INSTANCE.getRealmInstance().where(Author.class).count();
    }

    public void generateAuthors() {
        Realm realm = RealmSingleton.INSTANCE.getRealmInstance();
        if (getAuthorCount() == 0) {
            for (final Author author : AuthorGeneraterUtil.authors) {
                try {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            Author newAuthor = realm.createObject(Author.class, author.getId());
                            newAuthor.setName(author.getName());
                            newAuthor.setPoster(author.getPoster());
                            realm.copyToRealm(newAuthor);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Author getAuthorById(long id) {
        Realm realm = RealmSingleton.INSTANCE.getRealmInstance();
        return realm.where(Author.class).equalTo("id", id).findFirst();
    }

    public ArrayList<Author> getAllAuthors() {
        Realm realm = RealmSingleton.INSTANCE.getRealmInstance();
        return new ArrayList<>(realm.where(Author.class).findAll());
    }

    public ArrayList<Author> getAllAuthorsSortedByPostCount() {
        Realm realm = RealmSingleton.INSTANCE.getRealmInstance();
        return new ArrayList<>(realm.where(Author.class).sort("postCount", Sort.DESCENDING).findAll());
    }

    public long getRandomAuthorId() {
        RealmResults<Author> authors = RealmSingleton.INSTANCE.getRealmInstance().where(Author.class).findAll();
        if (authors.size() > 0)
            return authors.get(new Random().nextInt(authors.size())).getId();
        return 0;
    }
}
