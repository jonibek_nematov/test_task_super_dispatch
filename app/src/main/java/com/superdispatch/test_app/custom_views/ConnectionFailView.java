package com.superdispatch.test_app.custom_views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.superdispatch.test_app.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;


@EViewGroup(R.layout.connection_fail_view)
public class ConnectionFailView extends LinearLayout {

    private OnReloadClickListener onReloadClickListener;

    public ConnectionFailView(Context context) {
        super(context);
    }

    public ConnectionFailView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ConnectionFailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ConnectionFailView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    public void setOnReloadClickListener(OnReloadClickListener onReloadClickListener) {
        this.onReloadClickListener = onReloadClickListener;
    }

    @Click(R.id.btnReload)
    void reloadButtonClicked() {
        if (onReloadClickListener != null)
            onReloadClickListener.onReloadClick();
    }

    public interface OnReloadClickListener {
        void onReloadClick();
    }
}
