package com.superdispatch.test_app.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class ScrollListener extends RecyclerView.OnScrollListener {

    LinearLayoutManager layoutManager;

    public ScrollListener(LinearLayoutManager linearLayoutManager) {
        this.layoutManager = linearLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0) {
            int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
            int totalAdapterItemCount = layoutManager.getItemCount();
            if (totalAdapterItemCount <= (lastVisibleItem + 5)) {
                loadData();
            }
        }

    }

    public abstract void loadData();
}
