package com.superdispatch.test_app.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateFormatter {
    public static String formatDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd,yyyy HH:mm", Locale.ENGLISH);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        return MONTHS[calendar.get(Calendar.MONTH)] + " " + formatter.format(calendar.getTime());
    }

    private static String[] MONTHS = new String[]{
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
    };
}
