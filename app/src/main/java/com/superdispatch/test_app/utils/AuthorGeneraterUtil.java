package com.superdispatch.test_app.utils;

import com.superdispatch.test_app.R;
import com.superdispatch.test_app.model.Author;

public class AuthorGeneraterUtil {
    public static Author[] authors = new Author[]{
            new Author(1, "Begzod", R.drawable.ic_avatar_begzod),
            new Author(2, "Far", R.drawable.ic_avatar_far),
            new Author(3, "Fazliddin", R.drawable.ic_avatar_fazliddin),
            new Author(4, "Ibrohim", R.drawable.ic_avatar_ibrohim),
            new Author(5, "Iskander", R.drawable.ic_avatar_iskander),
            new Author(6, "Jahongir", R.drawable.ic_avatar_jahongir),
            new Author(7, "Kamola", R.drawable.ic_avatar_kamola),
            new Author(8, "Nodir", R.drawable.ic_avatar_nodir),
            new Author(9, "Nuriddin", R.drawable.ic_avatar_nuriddin),
            new Author(10, "Sukhrob", R.drawable.ic_avatar_sukhrob)};
}
