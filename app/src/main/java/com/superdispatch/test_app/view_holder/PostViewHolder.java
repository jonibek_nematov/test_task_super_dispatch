package com.superdispatch.test_app.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.superdispatch.test_app.R;
import com.superdispatch.test_app.database.AuthorDao;
import com.superdispatch.test_app.database.AuthorDaoImpl;
import com.superdispatch.test_app.model.Author;
import com.superdispatch.test_app.model.Post;

import static com.superdispatch.test_app.utils.DateFormatter.formatDate;

public class PostViewHolder extends RecyclerView.ViewHolder {

    private TextView tvPostAuthorName, tvPostPublishDate, tvPostTitle, tvPostContent;
    private ImageView ivPostAuthorAvatar;
    private AuthorDao authorDao;

    public PostViewHolder(View itemView) {
        super(itemView);
        authorDao = new AuthorDaoImpl();
        tvPostAuthorName = itemView.findViewById(R.id.tvPostAuthorName);
        tvPostPublishDate = itemView.findViewById(R.id.tvPostPublishDate);
        tvPostTitle = itemView.findViewById(R.id.tvPostTitle);
        tvPostContent = itemView.findViewById(R.id.tvPostContent);
        ivPostAuthorAvatar = itemView.findViewById(R.id.ivPostAuthorAvatar);
    }

    public void setValues(Post post) {
        if (post != null) {
            Author author = authorDao.getAuthorById(post.getAuthorId());
            tvPostAuthorName.setText(author.getName());
            tvPostPublishDate.setText(formatDate(post.getPublishedAt()));
            tvPostTitle.setText(post.getTitle());
            tvPostContent.setText(post.getContent());
            ivPostAuthorAvatar.setImageDrawable(itemView.getContext().getDrawable(author.getPoster()));
        }
    }


}

