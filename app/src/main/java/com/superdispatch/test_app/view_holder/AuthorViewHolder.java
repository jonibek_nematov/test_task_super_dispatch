package com.superdispatch.test_app.view_holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.superdispatch.test_app.R;
import com.superdispatch.test_app.model.Author;

import static java.lang.String.format;

public class AuthorViewHolder extends RecyclerView.ViewHolder {

    private TextView tvPostAuthorName, tvPostCount, tvIndex;
    private ImageView ivPostAuthorAvatar, ivMedal;
    private Context context;

    public AuthorViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        tvPostAuthorName = itemView.findViewById(R.id.tvPostAuthorName);
        tvPostCount = itemView.findViewById(R.id.tvPostCount);
        tvIndex = itemView.findViewById(R.id.tvIndex);
        ivPostAuthorAvatar = itemView.findViewById(R.id.ivPostAuthorAvatar);
        ivMedal = itemView.findViewById(R.id.ivMedal);
    }

    public void setValues(Author author, int index) {
        index++;
        if (author != null) {
            tvPostAuthorName.setText(author.getName());
            tvPostCount.setText(format(context.getResources().getQuantityString(R.plurals.number_of_jokes, (int) author.getPostCount()), author.getPostCount()));
            tvIndex.setText(String.valueOf(index));
            ivPostAuthorAvatar.setImageDrawable(itemView.getContext().getDrawable(author.getPoster()));
            setMedal(index);
        }
    }

    private void setMedal(int index) {
        ivMedal.setVisibility(View.VISIBLE);
        tvIndex.setTextColor(context.getResources().getColor(R.color.white));
        if (index == 1) {
            ivMedal.setImageDrawable(context.getDrawable(R.drawable.ic_gold));
        } else if (index == 2) {
            ivMedal.setImageDrawable(context.getDrawable(R.drawable.ic_silver));
        } else if (index == 3) {
            ivMedal.setImageDrawable(context.getDrawable(R.drawable.ic_bronze));
        } else {
            tvIndex.setTextColor(context.getResources().getColor(R.color.black));
            ivMedal.setVisibility(View.INVISIBLE);
        }
    }
}

