package com.superdispatch.test_app.model;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Author implements RealmModel {

    @PrimaryKey
    private long id;
    private String name;
    private int poster;
    private long postCount;

    public Author() {
    }

    public Author(long id, String name, int poster) {
        this.id = id;
        this.name = name;
        this.poster = poster;
        this.postCount = 0;
    }

    public long getPostCount() {
        return postCount;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPostCount(long postCount) {
        this.postCount = postCount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPoster(int poster) {
        this.poster = poster;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPoster() {
        return poster;
    }


}
