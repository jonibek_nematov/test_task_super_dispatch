package com.superdispatch.test_app.model;

import java.util.Random;

public class Jokes {

    public static String getRandomJoke(){
        return jokes[new Random().nextInt(jokes.length)];
    }

    public static String[] jokes = new String[]{
            "Moses had the first tablet that could connect to the cloud",
            "The first computer dates back to Adam and Eve. It was an Apple with limited memory, just one byte. And then everything crashed.",
            "Maybe if we start telling people the brain is an app they will start using it.",
            "My mom said that if I don't get off my computer and do my homework she'll slam my head on the keyboard, but I think she's jokinfjreoiwjrtwe4to8rkljreun8f4ny84c8y4t58lym4wthylmhawt4mylt4amlathnatyn",
            "You must be an angel, because your texture mapping is so divine!",
            "My email password has been hacked. That's the third time I've had to rename the cat.",
            "They should build the wall with Hillary's emails because nobody can get over them.",
            "We just got a fax. At work. We didn't know we had a fax machine. The entire department just stared at it. I poked it with a stick.",
            "Is your name Wi-Fi? Because I'm feeling a connection.",
            "Writing a horror screenplay. It starts off with a ringing phone. The person answers, and it's their mum saying \"I have a computer question.\"",
            "Entered what I ate today into my new fitness app and it just sent an ambulance to my house.",
            "A computer once beat me at chess, but it was no match for me at kick boxing.",
            "Doctor's office: All our records are electronic now just fill out these 12 forms.",
            "If I freeze, it's not a computer virus. I was just stunned by your beauty.",
            "I need more than 140 characters to tell you how beautiful you are.",
            "I think my neighbor is stalking me as she's been googling my name on her computer. I saw it through my telescope last night.",
            "So apparently RSVP'ing back to a wedding invite 'maybe next time' isn't the correct response.",
            "A clean house is the sign of a broken computer.",
            "I changed my password to \"incorrect\". So whenever I forget what it is the computer will say \"Your password is incorrect\".",
            "What did E.T.'s mother say to him when he got home? \"Where on Earth have you been?!\"",
            "I would like to thank everybody that stuck by my side for those five long minutes my house didn't have internet.",
            "A TV can insult your intelligence, but nothing rubs it in like a computer.",
            "I wonder what my parents did to fight boredom before the internet. I asked my 17 brothers and sisters and they didn't know either.",
            "You have the nicest syntax I've ever seen.",
            "I love the F5 key. It´s just so refreshing.",
            "Q: What do you call the security outside of a Samsung Store? A: Guardians of the Galaxy.",
            "If Bill Gates had a penny for every time I had to reboot my computer ...oh wait, he does.",
            "You're old enough to remember when emojis were called \"hieroglyphics.\"",
            "I changed my password to \"incorrect\", so anytime I forget and enter the wrong thing, the computer tells me what it is.",
            "I named my hard drive \"dat ass\" so once a month my computer asks if I want to 'back dat ass up'",
            "My boyfriend said he didn't have a date that same day I caught him eating one.",
            "It's ok computer, I go to sleep after 20 minutes of inactivity too.",
            "Well son, in the '90s, there was no drooling emoji. You had to show up at a girl's door and actually drool.",
            "Failure is not an option—it comes bundled with the software.",
            "I mostly just scroll through Instagram as a reminder of what brands I said out loud yesterday.",
            "Are you a keyboard? Because you're my type!",
            "Did you ever look at what's \"Popular on Netflix\" and think, man there are a lot of dumb motherfuckers watching Netflix?"
    };
}
