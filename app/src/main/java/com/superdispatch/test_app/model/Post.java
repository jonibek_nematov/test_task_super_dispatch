package com.superdispatch.test_app.model;

import com.superdispatch.test_app.database.AuthorDaoImpl;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Post implements Serializable, RealmModel {

    @PrimaryKey
    private long id;

    private String title;

    private String content;

    private Date publishedAt;

    private long authorId;

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getTitle() {
        return title;
    }

    public long getId() {
        return id;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public static class Builder {

        Post post;

        public Builder() {
            post = new Post();
        }

        public Builder setPostId(long id) {
            post.id = id;
            return this;
        }

        public Builder setPostTitle(String title) {
            post.title = title;
            return this;
        }


        public Builder setAuthorId(long authorId) {
            post.authorId = authorId;
            return this;
        }

        public Builder setDate(Date date) {
            post.publishedAt = date;
            return this;
        }

        public Builder setContent(String content) {
            post.content = content;
            return this;
        }

        public Post build() {
            return post;
        }
    }

    public static Post getPost(long i) {
        long authorId = new AuthorDaoImpl().getRandomAuthorId();
        return new Post.Builder()
                .setPostId(i)
                .setAuthorId(authorId)
                .setPostTitle("Joke #" + i)
                .setContent(Jokes.getRandomJoke())
                .setDate(new Date()).build();
    }


}
