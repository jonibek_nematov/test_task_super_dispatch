package com.superdispatch.test_app;

import android.app.Application;

import com.superdispatch.test_app.database.DatabasePostGenerator;

import io.realm.Realm;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initRealm();
        generateData();
    }

    //Initialize realm database instance
    public void initRealm() {
        Realm.init(this);
    }

    //Generates sample data when app is launched
    public void generateData() {
        new DatabasePostGenerator().generateData();
    }
}
