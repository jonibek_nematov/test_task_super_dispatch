Welcome to Joke generator app
Hi there, here is my app which i made as test task for Super Dispatch. Feel free to use it and criticize (NO!)

In this project i used two third party libaries :

1.[Realm.io](https://realm.io/docs/java/latest) - for non relational database;

2.[Android Annotations](https://github.com/androidannotations/androidannotations/wiki) - for view binding and working with threads (but did'not use, Realm.io has own tools)

Purpose of app is to solve problem which caused by pagination when you try to load rest of data while server is adding new items.

My solution is sending last loaded data to datasource to let it know which data it should send back to me.


[Demo apk](https://bitbucket.org/jonibek_nematov/test_task_super_dispatch/src/master/app/demo.apk)
Have fun!